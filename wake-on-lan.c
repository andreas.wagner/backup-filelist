#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>


void wake_on_lan(unsigned char * mac)
{
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(fd == -1)
	{
		printf("socket(): %i, %s\n", errno, strerror(errno));
	}
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(9);
	addr.sin_addr.s_addr =
		(255 << 24) +
		(255 << 16) +
		(255 << 8) +
		255;
	
	char buffer[6+16*6];
	memset(buffer, 0xff, 6);
	for(int i = 6; i < 6+16*6; i+=6)
	{
		memcpy(buffer + i, mac, 6);
	}
	
	int broadcastEnable=1;
	int ret = setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
	if(ret == -1)
	{
		printf("setsockopt(): %i, %s\n", errno, strerror(errno));
	}
	
	ssize_t result = sendto(fd, buffer, sizeof(buffer), 0, (struct sockaddr*)&addr, sizeof(addr));
	if(result == -1)
	{
		printf("sendto(): %i, %s\n", errno, strerror(errno));
	}
	close(fd);
}


int main(int argc, char *argv[])
{
	uint64_t value;
	unsigned char * ptr = &value;
	unsigned char mac[6];
	if(argc == 2)
	{
		if(strlen(argv[1]) != 12)
			return 1;
		value = strtoll(argv[1], NULL, 16);
		for(int i = 0; i < 6; i++)
		{
			mac[i] = *(ptr + 5-i);
		}
		wake_on_lan(mac);
	}
	return 0;
}
