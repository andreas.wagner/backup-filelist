#include <filesystem>
#include <fstream>
#include <list>
#include <map>
#include <iostream>
#include <sstream>


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


struct activity
{
	enum kind
	{
		CreateDir,
		CopyFile,
		ReplaceFile,
		RemoveFile
	} kind;
	
	activity(enum kind k, std::filesystem::path s):
		kind(k),
		source(s)
	{
	}
	std::filesystem::path source;
};

std::list<activity> Activities{};

std::map<std::filesystem::path, time_t> mod_times{};
std::filesystem::path backup_dir("./2backup/");


void enlist(std::filesystem::path p, std::ofstream &out);

void enlist(std::filesystem::path p, std::ofstream &out)
{
	struct stat s;
	
	
	if(is_directory(p))
	{
		lstat(p.string().c_str(), &s);
		out << p << s.st_mtime;
		if(mod_times.count(p) == 0)
		{
			Activities.emplace_back(activity::kind::CreateDir, p);
		}
		else
		{
			mod_times.erase(p);
		}
		std::filesystem::directory_iterator iter(p);
		for(auto & p : iter)
		{
			enlist(p.path(), out);
		}
	}
	else if(is_regular_file(p))
	{
		lstat(p.string().c_str(), &s);
		out << p << s.st_mtime;
		if(mod_times.count(p) == 0)
		{
			Activities.emplace_back(activity::kind::CopyFile, p);
		}
		else if(mod_times.count(p) > 0 && mod_times[p] < s.st_mtime)
		{
			Activities.emplace_back(activity::kind::ReplaceFile, p);
			mod_times.erase(p);
		}
		else
		{
			mod_times.erase(p);
		}
	}
}


int main(int argc, char *argv[])
{
	std::string filename1;
	std::string filename2;
	std::string backup_filename;
	std::string dirname;
	if(argc == 4)
	{
		filename1 = argv[1];
		filename2 = filename1 + std::string(".tmp");
		backup_filename = argv[2];
		dirname = argv[3];
		
	}
	else
		return 1;
	std::filesystem::path in_file(filename1);
	std::filesystem::path out_file(filename2);
	std::ifstream in(in_file);
	std::ofstream out(out_file);
	backup_dir = dirname;
	
	// Insert contents of file1
	while(in.good())
	{
		std::filesystem::path current;
		time_t time;
		in >> current >> time;
		mod_times[current] = time;
	}
	
	// go through directory and sync to file2
	enlist(backup_dir, out);
	
	// list files having been removed
	for(auto & p : mod_times)
	{
		Activities.emplace_back(activity::kind::RemoveFile, p.first);
	}
	
	// stop if no action necessary
	if(Activities.size() == 0)
	{		
		std::filesystem::remove(in_file);
		std::filesystem::rename(out_file, in_file);
		return 0;
	}
	
	std::stringstream files_to_del;
	std::stringstream dirs_to_create;
	
	
	std::stringstream zip_command;
	std::stringstream zip_filename;
	//std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	zip_filename << backup_filename;
	/* << "backup_" << std::put_time(
			std::localtime(&time)
				, "%FT%H%M%S"
			) << ".zip";*/
	zip_command << "zip -@ " << zip_filename.str();
	
	// run zip-command
	FILE* zip_fs = popen(zip_command.str().c_str(), "w");
	if(zip_fs != NULL)
	{
		for(auto & act : Activities)
		{
			int written = 0;
			switch(act.kind)
			{
			case activity::kind::CopyFile:
				std::cout << act.source << std::endl;
				written = fwrite(act.source.c_str(), act.source.string().size(), 1, zip_fs);
				if(written != 1)
					std::cerr << "written != 1" << std::endl;
				written = fwrite("\n", 1, 1, zip_fs);
				if(written != 1)
					std::cerr << "written != 1" << std::endl;
				break;
			case activity::kind::CreateDir:
				std::cout << "*" << act.source << std::endl;
				dirs_to_create << act.source.c_str() << " " ;
				break;
			case activity::kind::ReplaceFile:
				std::cout << act.source << std::endl;
				written = fwrite(act.source.c_str(), act.source.string().size(), 1, zip_fs);
				if(written != 1)
					std::cerr << "written != 1" << std::endl;
				written = fwrite("\n", 1, 1, zip_fs);
				if(written != 1)
					std::cerr << "written != 1" << std::endl;
				break;
			case activity::kind::RemoveFile:
				std::cout << "del " << act.source << std::endl;
				files_to_del << act.source.c_str() << " " ;
				break;
			default:
				abort();
			}
		}
		pclose(zip_fs);
	}
	
	std::filesystem::remove(in_file);
	std::filesystem::rename(out_file, in_file);
	return 0;
}
