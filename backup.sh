#!/bin/sh

ping -c 1 shellyplug-8ff4b6
if [ $? != 0 ]
then
	echo "shellyplug not available"
	exit 1
fi

BACKUP_FILENAME=backup_$(date +%Y-%m-%dT%H%M%SZ%z).zip
echo $BACKUP_FILENAME
cd /mnt/usbdisk/share/

sudo /usr/bin/touch backup-list
sudo /home/backup-to-server/bin/backup-filelist backup-list $BACKUP_FILENAME Gemeinsame\ Daten/


if [ ! -e $BACKUP_FILENAME ]
then
	echo "Nothing to do."
	exit
fi

#turn on power-supply
curl http://shellyplug-8ff4b6/relay/0?turn=on
sleep 5

# wake on lan; wake backup-server by mac-address
/home/backup-to-server/src/backup-filelist/build/wake-on-lan ecf4bbefc5c0

# wait for bootup
sleep 120

# copy backup-file to backup-server
STATUS=1
COUNT=0
while [ $STATUS != 0 -a \( $COUNT -lt 50 \) ]
do
	scp $BACKUP_FILENAME backup-server:
	STATUS=$?
	COUNT=$(($COUNT+1))
	sleep 5
done

sleep 3
ssh backup-server sudo shutdown -h 0

sleep 45

curl http://shellyplug-8ff4b6/relay/0?turn=off

cd
